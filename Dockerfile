FROM python:3

ENV loc ./yaml/config.yaml
ENV host localhost

COPY yaml ./yaml 
COPY stream.py .
COPY requirements.txt .
COPY twitter_secrets.py .

RUN pip install -r requirements.txt

CMD python -u stream.py --loc  $loc --host $host
