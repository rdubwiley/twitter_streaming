import os

import click
from influxdb import InfluxDBClient
import twitter
import yaml

from twitter_secrets import (
    api_key,
    api_secret_key,
    access_token,
    access_token_secret
)

api = twitter.Api(
    consumer_key=api_key,
    consumer_secret=api_secret_key,
    access_token_key=access_token,
    access_token_secret=access_token_secret
)

@click.command()
@click.option('--host', default='localhost', help='influxdb host')
@click.option('--port', default=8086, help='influxdb port')
@click.option('--database', default='twitter', help='database to push tweets to')
@click.option('--loc', default='yaml/config.yaml', help='location for streaming config yaml')
def main(
    host,
    port,
    database,
    loc
):
    client = InfluxDBClient(
        host,
        port,
        database=database
    )
    with open(loc, 'r') as f:
        doc = yaml.load(f, Loader=yaml.Loader)
    print("Running Streaming with doc: {}".format(doc))
    for i, line in enumerate(api.GetStreamFilter(stall_warnings=True, **doc)):
        if i%10 == 0:
            print("Streamed {} tweets so far".format(i))
        if line['coordinates']:
            if line['coordinates']['type'] == 'Point':
                lat = line['coordinates']['coordinates'][0]
                long = line['coordinates']['coordinates'][1]
            else:
                lat = 0.0
                long = 0.0
        else:
            lat = 0.0
            long = 0.0
        terms = []
        for term in doc['track']:
            if term.lower() in line['text'].lower():
                terms.append(term)
        hashtags = [ h['text'] for h in line['entities']['hashtags'] ]
        urls = [ u['url'] for u in line['entities']['urls'] ]
        mentions_usernames = [ un['name']  for un in line['entities']['user_mentions'] ]
        mentions_user_ids = [ str(uid['id'])  for uid in line['entities']['user_mentions'] ]
        json_body = [
            {
                'measurement': "tweet",
                "tags": {
                   "lang": line['lang'], 
                   "terms": "|".join(terms)
                },
                "time": line['created_at'],
                "fields": {
                    "tweet_id": line['id'],
                    "user_name": line['user']['name'],
                    "user_id": line['user']['id'],
                    "raw_text": line['text'],
                    "tweet_lat": lat,
                    "tweet_long": long,
                    "urls": "|".join(urls),
                    "mentions_usernames": "|".join(mentions_usernames),
                    "mentions_user_ids": "|".join(mentions_user_ids),
                    "hashtags": "|".join(hashtags)
                }
            }
        ]
        client.write_points(json_body)

if __name__ == '__main__':
    main()
